(function ($) {

Drupal.behaviors.editNoMore = {
  attach: function (context) {
    // Provide the vertical tab summary.
    $('fieldset#edit-editing', context).drupalSetSummary(function(context) {
      var vals = [];
      $("input[name^='edit_no_more_content_type']:checked", context).parent().each(function() {
        vals.push(Drupal.checkPlain($(this).text()));
      });
      $("input[name^='published']:checked", context).parent().each(function() {
        vals.push(Drupal.checkPlain($(this).text()));
      });
      return vals.join(', ');
    });
  }
};

})(jQuery);
