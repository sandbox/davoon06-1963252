Edit No More
================================================================================

Summary
--------------------------------------------------------------------------------

The Edit No More module provides an option for each content type to mark all of 
the nodes of this content type as uneditable after they have been published for 
the first time.


Requirements
--------------------------------------------------------------------------------

The Edit No More module has no requirement.


Installation
--------------------------------------------------------------------------------

1. Copy the edit_no_more folder to sites/all/modules or to a site-specific
   modules folder.
2. Go to Modules and enable the Edit No More module.


Configuration
--------------------------------------------------------------------------------

1. Go to Administration > Structure > Content types > my_content_type > edit >
   Publishing options and check the "Lock content after publishing" box.

2. Click on "Save content type"


Permissions
--------------------------------------------------------------------------------

The permission "Edit locked content" allows granted users to bypass node edition
denial (user with ID#1 allways bypasses acces control).


Known issues
--------------------------------------------------------------------------------

There is no known issue at the moment.


Support
--------------------------------------------------------------------------------

Please post bug reports and feature requests in the issue queue:

  http://drupal.org/project/issues/1963252


Credits
--------------------------------------------------------------------------------

Author: David Tischmacher <david.tischmacher@gmail.com>
